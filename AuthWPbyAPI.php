<?php
/**
 * Authentification via WordPress Plugin for LimeSurvey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2014 Denis Chenu <http://sondages.pro>
 * @copyright 2014 Bruce Mahillet de Komet <http://jevaluemaformation.com>
 * @license GPL v3
 * @version 1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
 
class AuthWPbyAPI extends AuthPluginBase
{
    protected $storage = 'DbStorage';    
    
    static protected $description = 'A plugin to authenticate user via WordPress (direct). Wordpress mus be in same domain of LimeSurvey (and better limesurvey in a subdirectory of wordpress).';
    static protected $name = 'Authentification via WordPress';

    protected $settings = array(
        'authwp_dir' => array(
            'type' => 'string',
            'label' => 'The directory where WP is (used only for direct access).',
            'default' => '../'
        ),
        'authwp_dbhost' => array(
            'type' => 'string',
            'label' => 'WordPress DB Host (default to LimeSurvey DB Host)'
        ),
        'authwp_dbport' => array(
            'type' => 'string',
            'label' => 'WordPress DB Port (default to LimeSurvey DB Port or 3306 id name or host is define)'
        ),
        'authwp_dbname' => array(
            'type' => 'string',
            'label' => 'WordPress DB Name  (default to LimeSurvey DB Name)'
        ),
        'authwp_dbuser' => array(
            'type' => 'string',
            'label' => 'WordPress DB User (default to LimeSurvey DB User)'
        ),
        'authwp_dbpassword' => array(
            'type' => 'string',
            'label' => 'WordPress DB User password (default to LimeSurvey DB User)'
        ),
        'authwp_dbprefix' => array(
            'type' => 'string',
            'label' => 'WordPress DB prefix',
            'default' => 'wp_'
        ),
        'authwp_userpass' => array(
            'type' => 'checkbox',
            'label' => 'Allow connection by user/pass'
        ),
        'authwp_cookies' => array(
            'type' => 'checkbox',
            'label' => 'Test cookies access (need LimeSurvey and wordpress in same subdomain).'
        ),
        'authwp_default' => array(
            'type' => 'checkbox',
            'label' => 'Check to make default authentication method'
        ),
        'authwp_redirect' => array(
            'type' => 'string',
            'label' => 'Redirection if cookies authentification and default is WordPress (put an url redirect to this url if user is not loggued in wordpress).'
        ),
        'authwp_autocreate' => array(
            'type' => 'checkbox',
            'label' => 'Auto create user.',
            'default' => true
        ),
    );

    protected $sWpLoad = false;
    private $sAuthWpMethod="";
    public function __construct(PluginManager $manager, $id) {
        parent::__construct($manager, $id);
        
        /**
         * Here you should handle subscribing to the events your plugin will handle
         */
        $this->subscribe('beforeLogin');
        $this->subscribe('newLoginForm');
        $this->subscribe('newUserSession');
        $this->subscribe('afterLoginFormSubmit');
        $this->subscribe('beforeActivate');
    }

    public function beforeActivate()
    {
        $oEvent = $this->getEvent();
#        die("<pre>".print_r(Yii::app()->db,true)."</pre>");
#        $sWPdirectory = Yii::app()->getConfig('rootdir').DIRECTORY_SEPARATOR.$this->get('authwp_dir');
#        // Get configuration settings:
        $this->addWpDb();
        // Test if connection is OK
        $sWpDbPrefix=$this->get('authwp_dbprefix');
        if(!in_array($sWpDbPrefix.'options',Yii::app()->wpdb->schema->getTableNames())){
            $oEvent->set('success', false);
            $oEvent->set('message','Please review your wordpress DB connection parameters.');
        }else{
            $oEvent->set('success', true);
        }
    }

    public function beforeLogin()
    {
        $oEvent = $this->getEvent();
        $this->addWpDb();
        // Allow by cookie
        if($this->get('authwp_cookies')){
            $aUser=$this->getWpCookieUser();
            //die("<pre>".print_r($aUser,true)."</pre>");
            if($aUser){
                $oUser = $this->api->getUserByName($aUser['user_login']);
                if($oUser || $this->get('authwp_autocreate')){
                    $this->setUsername($aUser['user_login']);
                    $this->sAuthWpMethod="cookies";
                    $this->setAuthPlugin(); // This plugin handles authentication, halt further execution of auth plugins
                    return;
                }
            }
        }
        // Redirect if not connected
        if($this->get('authwp_cookies') && $this->get('authwp_default') && $this->get('authwp_redirect')){

            $sRedirectUrl=$this->get('authwp_redirect');
            if(in_array(parse_url($sRedirectUrl, PHP_URL_SCHEME),array('http','https'))){
                if(filter_var($sRedirectUrl, FILTER_VALIDATE_URL) !== false){
                    //header('Status: 401 Unauthorized', false, 401);
                    header("Location: $sRedirectUrl");
                    exit();
                }
            }
        }
        // Default by DB
        if ($this->get('authwp_default') && $this->get('authwp_userpass'))
        {
            $this->getEvent()->set('default', get_class($this));
        }
    }

    public function newLoginForm()
    {
        if($this->get('authwp_userpass')){
            $this->getEvent()->getContent($this)
                 ->addContent(CHtml::tag('li', array(), "<label for='user'>"  . gT("Username") . "</label><input name='user' id='user' type='text' size='40' maxlength='40' value='' />"))
                 ->addContent(CHtml::tag('li', array(), "<label for='password'>"  . gT("Password") . "</label><input name='password' id='password' type='password' size='40' maxlength='40' value='' />"));
        }
    }

    public function afterLoginFormSubmit()
    {
        // Allways (trying to reset password if user exist in DB ????)
        $request = $this->api->getRequest();
        if ($request->getIsPostRequest()) {
            $this->setUsername( $request->getPost('user'));
            $this->setPassword($request->getPost('password'));
        }
    }

    public function newUserSession()
    {
        $sUserName = $this->getUserName();
        $sUserPass = $this->getPassword();
        $oUser = $this->api->getUserByName($sUserName);
        
        if (is_null($oUser) && $this->get('authwp_autocreate'))
        {
            if($this->getWpCookieUser() && $this->get('authwp_cookies'))
            {
                $aUser=$this->getWpCookieUser();
                $oUser=new User;
                $oUser->users_name=$aUser['user_login'];
                $oUser->full_name=$aUser['display_name'];
                $oUser->password=substr(md5(rand()),0,20);;
                $oUser->parent_id=1;
                $oUser->lang='auto';
                $oUser->email=$aUser['user_email'];
            }
            elseif($this->getWpDbUser($sUserName,$sUserPass) && $this->get('authwp_userpass'))
            {
                $aUser=$this->getWpDbUser($sUserName,$sUserPass);
                $oUser=new User;
                $oUser->users_name=$aUser['user_login'];
                $oUser->full_name=$aUser['display_name'];
                $oUser->password=substr(md5(rand()),0,20);;
                $oUser->parent_id=1;
                $oUser->lang='auto';
                $oUser->email=$aUser['user_email'];
            }
            else// Invalid user
            {
                $this->setAuthFailure(self::ERROR_USERNAME_INVALID);
                return;
            }
            if ($oUser->save())
            {
                // $aUser['level']
                if((int)$aUser['user_level']>=9){
                    $aPermission=Array(
                        'superadmin' => array('read'=>true),
                    );
                }else{
                    $aPermission=Array(
                        'surveys' => array('create'=>true,'export'=>true),
                        'template' => array('read'=>true),
                        'labelsets' => array('read'=>true,'export'=>true),
                        'participantpanel' => array('create'=>true,'read'=>true,'update'=>true,'delete'=>true),
                    );
                }
                $permission=new Permission;
                $permission->setPermissions($oUser->uid, 0, 'global', $aPermission, true);

                // read again user from newly created entry
                $this->setAuthSuccess($oUser);
                return;
            }else{
                $this->setAuthFailure("DB error");
                return;
            }
        } else {
            if($this->getWpCookieUser() && $this->get('authwp_cookies'))
            {
                $oUser->password=substr(md5(rand()),0,20);
                $oUser->save();
                $this->setAuthSuccess($oUser);
                return;
            }
            elseif($this->getWpDbUser($sUserName,$sUserPass) && $this->get('authwp_userpass'))
            {
                $oUser->password=substr(md5(rand()),0,20);
                $oUser->save();
                $this->setAuthSuccess($oUser);
                return;
            }
            else
            {
                // Test if user is in Worpress
                $aUser = Yii::app()->wpdb->createCommand()
                                        ->select('user_login,user_nicename,user_email,display_name')
                                        ->from('{{users}}')
                                        ->andWhere("user_login = :user_login")
                                        ->bindParam(':user_login',$sUserName)
                                        ->queryRow();
                if($aUser){
                    $oUser->password=substr(md5(rand()),0,20);
                    $oUser->full_name=$aUser['user_nicename'];
                    $oUser->email=$aUser['user_email'];
                    $oUser->save();
                }
                $this->setAuthFailure(self::ERROR_USERNAME_INVALID);
                return;
            }
        }
    }

    /**
    * Try to validate by WordPress COOKIE
    * Please : use at your own risk : not tested with plugin, configuration etc ....
    * return array : User information
    **/
    private function getWpCookieUser()
    {
        static $aWpUser;
        if(isset($aWpUser))
            return $aWpUser;
        // We can not include wordpress config file, because he include more than needed (and create class)
        // Get the COOKIEHASH
        $aSiteurl = Yii::app()->wpdb->createCommand()
                                ->select('option_value')
                                ->from('{{options}}')
                                ->where("option_name ='siteurl' ")
                                ->queryRow();
        $siteurl=$aSiteurl['option_value'];
        if ( $siteurl )
            $sCookieHash=md5( $siteurl );
        else
            $sCookieHash="";
        $wpcookie=false;
        if(isset($_COOKIE['wordpress_logged_in_' . $sCookieHash]) && $_COOKIE['wordpress_logged_in_' . $sCookieHash])
            $wpcookie = $_COOKIE['wordpress_logged_in_' . $sCookieHash];
        if (!$wpcookie && isset($_COOKIE['wordpress_' . $sCookieHash]) && $_COOKIE['wordpress_' . $sCookieHash] )
            $wpcookie = $_COOKIE['wordpress_' . $sCookieHash];
        if (!$wpcookie )
            return;
        $wpcookie_elements = explode('|', $wpcookie);
        if ( count($wpcookie_elements) != 3 )
            return;
        $aCookie=array();
        list($aCookie['username'], $aCookie['expiration'], $aCookie['hmac']) = $wpcookie_elements;

        if ( $aCookie['expiration'] < time() )
            return;
        // OK : then get the user information
        $aWpUser = Yii::app()->wpdb->createCommand()
                                ->select('user_login,user_pass,user_nicename,user_email,display_name,ul.meta_value as user_level,ac.meta_value as active,to.meta_value as tos')
                                ->from('{{users}}')
                                ->leftJoin('{{usermeta}} ul', 'ID = ul.user_id AND ul.meta_key="wp_user_level"')
                                ->leftJoin('{{usermeta}} ac', 'ID = ac.user_id AND ac.meta_key="active"')
                                ->leftJoin('{{usermeta}} to', 'ID = to.user_id AND to.meta_key="tos"')
                                ->andWhere("user_login = :user_login")
                                ->bindParam(':user_login',$aCookie['username'])
                                ->queryRow();
	    if ( ! $aWpUser ) {
		    return false;
	    }
	    // Get the code from WordPress authentification by cookie
	    $pass_frag = substr($aWpUser['user_pass'], 8, 4);
	    $key = self::wp_hash($aCookie['username'] . $pass_frag . '|' . $aCookie['expiration'], 'logged_in');
	    $hash = self::hash_hmac('md5', $aCookie['username'] . '|' . $aCookie['expiration'], $key);
	    if($hash!=$aCookie['hmac'])
		    return;
        // We are here, return user \o/
        return $aWpUser;
    }
    /**
    * Validate user by username/password from WordPress
    * @param string $sUserName : the user name
    * @param string $sUserPass : the user pass
    * return array : User information
    **/
    private function getWpDbUser($sUserName,$sUserPass)
    {
        $this->addWpDb();
        $aUser = Yii::app()->wpdb->createCommand()
                                ->select('user_login,user_pass,user_nicename,user_email,display_name,ul.meta_value as user_level,ac.meta_value as active,to.meta_value as tos')
                                ->from('{{users}}')
                                ->leftJoin('{{usermeta}} ul', 'ID = ul.user_id AND ul.meta_key="wp_user_level"')
                                ->leftJoin('{{usermeta}} ac', 'ID = ac.user_id AND ac.meta_key="active"')
                                ->leftJoin('{{usermeta}} to', 'ID = to.user_id AND to.meta_key="tos"')
                                ->andWhere("user_login = :user_login")
                                ->bindParam(':user_login',$sUserName)
                                ->queryRow();
        if(!$aUser)
            return;
        if($aUser['active']=="0")
            return;
        //Yii::import('plugins.AuthWPbyAPI.third_party.phpass.PasswordHash');
        require_once dirname(__FILE__).'/third_party/phpass/PasswordHash.php';// DIRECTORY_SEPARATOR not needed
        $oHasher = new PasswordHash(8, TRUE);
        $bCheck = $oHasher->CheckPassword($sUserPass, $aUser['user_pass']);
        if($bCheck)
            return $aUser;
        else
            return;
    }
    /**
    * Add the db from plugin configuration in new Yii db
    **/
    private function addWpDb()
    {
        //Copy some part of wp-config
        define('AUTH_KEY','E7J3_Fj2dtwzibeq>#498O<YRh(A*IY#$:c4wH`.N|C,FB7/%y2&Ar1%,LqXMNYf');
        define('SECURE_AUTH_KEY','&fGj#HK|WRTR6~0;Fsza}Sy_ayuU<}lG]eVE0w-Z3+L&)*~tg+VfUD0q`+uu!VHn');
        define('LOGGED_IN_KEY','*4F-CaHn{?u][xG+R0bKrHot5j&X? wN?-2,)w`$e#X@B>KDUp(QtHfp3U+XZ:%q');
        define('NONCE_KEY',',|&l5#~(*_s/Cnex>QA9}|;#T(,gp*u{^|-uaU%WH701oYJDylNuYuB^[t#ah0D!');
        define('AUTH_SALT','o-U|1X)k_t3>iM>Q9D$fma@+!et>[a>8--cCO|7>1}#&!sM%@xwf8_z{nD+{y|J_');
        define('SECURE_AUTH_SALT','Fl10F)U.gef-HDAp^fN5+?|K])cG1Vav` 21N<8+b6KlgkDJNZL0R0[R r3 F+#5');
        define('LOGGED_IN_SALT','aI/[#@OQUO6nO1suV]M(]21FhrJSCn*K`8ZM.B9}qEgDL)eWpZZrObh U^Yocdd7');
        define('NONCE_SALT','N7 f0/1MM T!0uhC2v}yIofR~yErG[UoMO=r~kmxqq&b^-Q&;p-wr0jQ_s}h<u)_');
        $bExist=Yii::app()->getComponent('wpdb',false);
        if($bExist)// If already exist 
            return;
        $sWpDbHost      = $this->get('authwp_dbhost');
        $sWpDbPort      = $this->get('authwp_dbport');
        $sWpDbName      = $this->get('authwp_dbname');
        $sWpDbUser      = $this->get('authwp_dbuser');
        $sWpDbPassword  = $this->get('authwp_dbpassword');
        $sWpDbPrefix    = $this->get('authwp_dbprefix');
        if($sWpDbHost || $sWpDbPort || $sWpDbName){
            if(!$sWpDbPort)
                $sWpDbPort="3306";
            $sConnectionString="mysql:host={$sWpDbHost};port={$sWpDbPort};dbname={$sWpDbName}";
        }else{
            $sConnectionString=Yii::app()->db->connectionString;
        }
        if(!$sWpDbUser)
            $sWpDbUser=Yii::app()->db->username;
        if(!$sWpDbPassword)
            $sWpDbPassword=Yii::app()->db->password;
        $wpdb = Yii::createComponent(array(
           'class' => 'CDbConnection',
             'connectionString'=>$sConnectionString,
                'username'=>$sWpDbUser,
                'password'=> $sWpDbPassword,
                'charset'=>'utf8',
                'emulatePrepare' => true,
                'tablePrefix' => $sWpDbPrefix,
        ));
        Yii::app()->setComponent('wpdb', $wpdb);
    }


    /**
    * Some functions get from WordPress core : some plugins can broke this system
    * Not really validate 
    **/
    private static function wp_hash($data, $scheme = 'auth') {
	    $salt = self::wp_salt($scheme);
	    return self::hash_hmac('md5', $data, $salt);
    }
    private static function hash_hmac($algo, $data, $key){
	    $packs = array('md5' => 'H32', 'sha1' => 'H40');
	    if ( !isset($packs[$algo]) )
		    return false;
	    $pack = $packs[$algo];
	    if (strlen($key) > 64)
		    $key = pack($pack, $algo($key));

	    $key = str_pad($key, 64, chr(0));

	    $ipad = (substr($key, 0, 64) ^ str_repeat(chr(0x36), 64));
	    $opad = (substr($key, 0, 64) ^ str_repeat(chr(0x5C), 64));

	    $hmac = $algo($opad . pack($pack, $algo($ipad . $data)));
	    return $hmac;
    }
    private static function wp_salt( $scheme = 'auth' ) {

        static $duplicated_keys;
        if ( null === $duplicated_keys ) {
	        $duplicated_keys = array( 'put your unique phrase here' => true );
	        foreach ( array( 'AUTH', 'SECURE_AUTH', 'LOGGED_IN', 'NONCE', 'SECRET' ) as $first ) {
		        foreach ( array( 'KEY', 'SALT' ) as $second ) {
			        if ( ! defined( "{$first}_{$second}" ) )
				        continue;
			        $value = constant( "{$first}_{$second}" );
			        $duplicated_keys[ $value ] = isset( $duplicated_keys[ $value ] );
		        }
	        }
        }

        $key = $salt = '';
        if ( defined( 'SECRET_KEY' ) && SECRET_KEY && empty( $duplicated_keys[ SECRET_KEY ] ) )
	        $key = SECRET_KEY;
        if ( 'auth' == $scheme && defined( 'SECRET_SALT' ) && SECRET_SALT && empty( $duplicated_keys[ SECRET_SALT ] ) )
	        $salt = SECRET_SALT;

        if ( in_array( $scheme, array( 'auth', 'secure_auth', 'logged_in', 'nonce' ) ) ) {
	        foreach ( array( 'key', 'salt' ) as $type ) {
		        $const = strtoupper( "{$scheme}_{$type}" );
		        if ( defined( $const ) && constant( $const ) && empty( $duplicated_keys[ constant( $const ) ] ) ) {
			        $$type = constant( $const );
		        } elseif ( ! $$type ) {
			        $$type = self::get_site_option( "{$scheme}_{$type}" );
		        }
	        }
        } else {
	        if ( ! $key ) {
		        $key = self::get_site_option( 'secret_key' );
	        }
	        $salt = hash_hmac( 'md5', $scheme, $key );
        }

        $cached_salts[ $scheme ] = $key . $salt;
        return $cached_salts[ $scheme ];
    }
    
    private static function get_site_option($option){
        $aOption = Yii::app()->wpdb->createCommand()
                                ->select('option_value')
                                ->from('{{options}}')
                                ->where("option_name =:option ")
                                ->bindParam(':option',$option)
                                ->queryRow();
        return $aOption['option_value'];
    }
}
